package net._7colorlotus.gateway.routerPredicateFactory;

import java.time.ZonedDateTime;

/**
 * 配置谓词属性值
 */
public class MyConfig {
    private String key;
    private String value;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public static void main(String[] args) {
        System.out.println(ZonedDateTime.now());
    }
}
