package net._7colorlotus.gateway.filters;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Component
public class MyCalGatewayFilterFactory extends AbstractGatewayFilterFactory<MyConfig> {

    public MyCalGatewayFilterFactory(){
        super(MyConfig.class);
    }

    @Override
    public GatewayFilter apply(MyConfig config) {
        return new GatewayFilter() {

            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                long startTime = System.currentTimeMillis();
                System.out.println(config.getKey() + ":" + config.getValue());
                return chain.filter(exchange).then(
                        Mono.fromRunnable(() -> {
                            System.out.println(System.currentTimeMillis() - startTime);
                        })
                );
            }
        };
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("key", "value");
    }


}
