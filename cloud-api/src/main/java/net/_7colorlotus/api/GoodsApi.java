package net._7colorlotus.api;

import net._7colorlotus.entity.Goods;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("cloud-goods")
@RequestMapping("goods")
public interface GoodsApi {
    @RequestMapping("findById/{id}")
    Goods findById(@PathVariable("id") Integer id);

    @RequestMapping("b")
    String b();

    @GetMapping("kcc")
    String kcc(@RequestParam("productId") Integer productId, @RequestParam("stock") Integer stock);
}