package net._7colorlotus.goods.dao;


import net._7colorlotus.goods.model.Order;
import net._7colorlotus.goods.util.MyMapper;

public interface OrderMapper extends MyMapper<Order> {
}