package net._7colorlotus.goods.dao;

import net._7colorlotus.goods.model.Product;
import net._7colorlotus.goods.util.MyMapper;

public interface ProductMapper extends MyMapper<Product> {

}