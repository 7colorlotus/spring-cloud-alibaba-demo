package net._7colorlotus.goods.controller;

import net._7colorlotus.entity.Goods;
import net._7colorlotus.goods.service.ProductService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("goods")
public class GoodsController {


    @RequestMapping("findById/{id}")
    public Goods findById(@PathVariable("id") Integer id, HttpServletRequest request, @RequestHeader String token){
        System.out.println("token = " + token);
        System.out.println("findById id : " + id + ", request.getHeader(\"source\") : " + request.getHeader("source"));
        return new Goods("price", 10D);
    }


    @RequestMapping("b")
    public String b(){
        System.out.println("b");
        return "b";
    }

    @Resource
    private ProductService productService;

    @GetMapping("kcc")
    public String kcc(@RequestParam("productId") Integer productId, @RequestParam("stock") Integer stock){
        productService.kcc(productId, stock);

        return "success";
    }
}
