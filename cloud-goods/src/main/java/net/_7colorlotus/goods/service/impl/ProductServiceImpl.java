package net._7colorlotus.goods.service.impl;

import net._7colorlotus.goods.dao.ProductMapper;
import net._7colorlotus.goods.model.Product;
import net._7colorlotus.goods.service.ProductService;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ProductServiceImpl implements ProductService {
    @Resource
    private ProductMapper productMapper;

    @Override
    public void kcc(Integer productId, Integer stock){
        Product product = productMapper.selectByPrimaryKey(productId);
        product.setStock(product.getStock() - stock);
        System.out.println(1/0);
        productMapper.updateByPrimaryKey(product);
    }
}
