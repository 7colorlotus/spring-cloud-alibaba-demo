package net._7colorlotus.goods.configuration;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRibbonRule {

    @Bean
    public IRule getRule(){
        return new RoundRobinRule();
    }
}
