package net._7colorlotus.order.dao;


import net._7colorlotus.order.model.Order;
import net._7colorlotus.order.util.MyMapper;

public interface OrderMapper extends MyMapper<Order> {
}