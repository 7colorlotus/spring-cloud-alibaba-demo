package net._7colorlotus.order.dao;

import net._7colorlotus.order.model.Product;
import net._7colorlotus.order.util.MyMapper;

public interface ProductMapper extends MyMapper<Product> {

}