package net._7colorlotus.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import rule.MyRibbonRule;
import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
@EnableDiscoveryClient
@RibbonClient(name = "cloud-order", configuration = {MyRibbonRule.class}) //name指定哪个服务，需要使用configuration指定的负责均衡策略
@MapperScan("net._7colorlotus.order.dao")

@EnableFeignClients(basePackages = {"net._7colorlotus.api"})
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    @Bean
    @LoadBalanced //添加 ribbon 注解 才可以直接使用服务名访问
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
