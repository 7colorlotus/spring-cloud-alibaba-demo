package net._7colorlotus.order.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import net._7colorlotus.api.GoodsApi;
import net._7colorlotus.entity.Goods;
import net._7colorlotus.order.sentinel.OrderBlockHandler;
import net._7colorlotus.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
@RequestMapping("order")
@RefreshScope
public class OrderController {

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("saveOrder")
    public String saveOrder(){
        //硬编码方式请求其他服务
//        String url = "http://localhost:9001/goods/findById/1";

        //使用服务名方式
        String url = "http://cloud-goods/goods/findById/1";


        Goods goods = restTemplate.getForObject(url, Goods.class);
        System.out.println(goods);
        return "success";
    }

    @Value("${data.name}")
    private String dataName;

    @Autowired
    GoodsApi goodsApi;

    @RequestMapping("saveOrderUseOpenfeign")
    public String saveOrderUseOpenfeign(){
        Goods goods = goodsApi.findById(1);
        System.out.println(goods);
        return "success";
    }

    @GetMapping("test")
    public String testConfigVal(){
        System.out.println(dataName);
        return dataName;
    }

    @GetMapping("test2")
    public String test2ConfigVal(){
        System.out.println(dataName  + "test2");
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataName + "test2";
    }

    @GetMapping("test3")
    public String test3(@RequestParam(value = "flag", required = false) Integer flag){
        System.out.println(dataName  + "test2");
        if(null == flag){
            throw new IllegalArgumentException();
        }
        return dataName + "test2";
    }

    @Resource
    private OrderService orderService;

    @RequestMapping("saveOrderByService1")
    public String saveOrderByService(){
        System.out.println(orderService.saveOrder());
        return "success";
    }

    @RequestMapping("saveOrderByService2")
    public String saveOrderByService2(){
        System.out.println(orderService.saveOrder());
        return "success";
    }


    @GetMapping("test_exception1")
    @SentinelResource(value = "test_exception1", blockHandler = "handlerException")
    public String testException1(@RequestParam(value = "flag", required = false) Integer flag){
        System.out.println(dataName  + "test2");
        if(null == flag){
            throw new IllegalArgumentException();
        }
        return dataName + "test2";
    }


    public String handlerException(Integer flag, BlockException be){
        return "handlerException success";
    }

    /**
     * blockHandlerClass 指定控制类
     * blockHandler 指定控制方法
     * @param flag
     * @return
     */
    @GetMapping("test_exception2")
    @SentinelResource(value = "test_exception2", blockHandlerClass = OrderBlockHandler.class, blockHandler = "block")
    public String testException2(@RequestParam(value = "flag", required = false) Integer flag){
        System.out.println(dataName  + "test2");
        if(null == flag){
            throw new IllegalArgumentException();
        }
        return dataName + "test2";
    }

    /**
     * 必须使用注解 @SentinelResource
     * @param flag
     * @return
     */
    @RequestMapping("testGlobalExceptionHandler")
    @SentinelResource(value = "testGlobalExceptionHandler")
    public String testGlobalExceptionHandler(@RequestParam(value = "flag", required = false) Integer flag){
        System.out.println( "testGlobalExceptionHandler");
        if(null == flag){
            throw new IllegalArgumentException();
        }
        return "testGlobalExceptionHandler method success";
    }


    @RequestMapping("a")
    public String a(){
        System.out.println("a");
        System.out.println("goodsApi.b() return " + goodsApi.b());
        return "a";
    }


    @GetMapping("addOrder/{productId}")
    public String addOrder(@PathVariable("productId") Integer productId){
        System.out.println("productId " + productId);
        orderService.createOrder(productId);
        return "success";
    }
}
