package net._7colorlotus.order.sentinel.pull;

import java.util.HashMap;
import java.util.Map;

/**
 * 持久化规则常量
 */
public class PersistenceRuleConstant {

    /**
     * 存储文件路径
     */
    // /Users/shengzhu/Job/LocalDemo/spring-cloud-alibaba-demo/cloud-order
//    public static final String storePath = System.getProperty("user.home")+"\\sentinel\\rules\\";

    public static final String storePath = "/Users/shengzhu/Job/LocalDemo/spring-cloud-alibaba-demo/cloud-order/sentinel/rules/";


    /**
     * 各种存储sentinel规则映射map
     */
    public static final Map<String,String> rulesMap = new HashMap<String,String>();

    //流控规则文件
    public static final String FLOW_RULE_PATH = "flowRulePath";

    //降级规则文件
    public static final String DEGRAGE_RULE_PATH = "degradeRulePath";

    //授权规则文件
    public static final String AUTH_RULE_PATH = "authRulePath";

    //系统规则文件
    public static final String SYSTEM_RULE_PATH = "systemRulePath";

    //热点参数文件
    public static final String HOT_PARAM_RULE = "hotParamRulePath";

    static {
        rulesMap.put(FLOW_RULE_PATH,storePath+"flowRule.json");
        rulesMap.put(DEGRAGE_RULE_PATH,storePath+"degradeRule.json");
        rulesMap.put(SYSTEM_RULE_PATH,storePath+"systemRule.json");
        rulesMap.put(AUTH_RULE_PATH,storePath+"authRule.json");
        rulesMap.put(HOT_PARAM_RULE,storePath+"hotParamRule.json");
    }
}