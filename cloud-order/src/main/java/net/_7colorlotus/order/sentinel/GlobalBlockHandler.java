package net._7colorlotus.order.sentinel;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalBlockHandler {

    /**
     * 权限规则统一异常处理方法
     * @param e
     * @return
     */
    @ExceptionHandler(value = {AuthorityException.class})
    public String authorityExceptionHandler(AuthorityException e) {
        return "authorityExceptionHandler success";
    }

    /**
     * 熔断降级统一异常处理方法
     * @param e
     * @return
     */
    @ExceptionHandler(value = {DegradeException.class})
    public String degradeExceptionHandler(DegradeException e) {
        return "degradeExceptionHandler success";
    }

    /**
     * 流控异常统一处理方法
     * @param e
     * @return
     */
    @ExceptionHandler(value = {FlowException.class})
    public String flowExceptionHandler(FlowException e) {
        return "flowExceptionHandler success";
    }

    /**
     * 热点Key规则异常统一处理方法
     * @param e
     * @return
     */
    @ExceptionHandler(value = {ParamFlowException.class})
    public String paramFlowExceptionHandler(ParamFlowException e) {
        return "paramFlowExceptionHandler success";
    }

    /**
     * 系统保护规则异常统一处理方法
     * @param e
     * @return
     */
    @ExceptionHandler(value = {SystemBlockException.class})
    public String systemBlockExceptionHandler(SystemBlockException e) {
        return "systemBlockExceptionHandler success";
    }

    /**
     * 所有的sentinel异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = {BlockException.class})
    public String systemBlockExceptionHandler(BlockException e) {
        return "systemBlockExceptionHandler success";
    }
}
