package net._7colorlotus.order.service;

public interface OrderService {
    String saveOrder();

    String createOrder(Integer productId);
}
