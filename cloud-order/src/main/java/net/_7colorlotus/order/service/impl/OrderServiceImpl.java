package net._7colorlotus.order.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import io.seata.spring.annotation.GlobalTransactional;
import net._7colorlotus.api.GoodsApi;
import net._7colorlotus.order.dao.OrderMapper;
import net._7colorlotus.order.model.Order;
import net._7colorlotus.order.service.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderMapper orderMapper;

    @Resource
    private GoodsApi goodsApi;

    @SentinelResource("saveOrderMethod")
    @Override
    public String saveOrder() {
        System.out.println("save Order by Service");
        return "ok";
    }

    @Override
    @GlobalTransactional
    public String createOrder(Integer productId) {
        Order order = new Order();
        order.setAmount(new BigDecimal(100));
        order.setCreatedAt(new Date());
        order.setProductId(productId);
        orderMapper.insert(order);

        goodsApi.kcc(productId, 1);

        return "ok";
    }
}
